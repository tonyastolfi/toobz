

#include <toobz/int_types.hpp>
#include <toobz/protocol/ethernet.hpp>
#include <toobz/raw_socket.hpp>

#include <boost/asio/io_context.hpp>
#include <boost/format.hpp>

#include <glog/logging.h>

#include <iostream>

using namespace toobz::int_types;

int main(int /*argc*/, char** /*argv*/)
{
    std::cout << "The internet is indeed a series of toobz." << std::endl;

    std::string iface = "enp6s0";

    boost::asio::io_context io;

    LOG(INFO) << "creating socket";

    toobz::RawSocket socket{io};

    LOG(INFO) << "open socket";

    socket.open(toobz::RawSocketProtocol{});

    LOG(INFO) << "bind socket";

    socket.bind(toobz::RawSocketEndpoint{iface});

    LOG(INFO) << "run";

    //    io.run();

    LOG(INFO) << "done!";

    //    socket.non_blocking();

    toobz::MACAddress filter_addr = BATT_OK_RESULT_OR_PANIC(toobz::MACAddress::from_str("80:6d:97:24:92:ec"));
    for (;;) {
        std::array<u8, 2048> buffer;

        [[maybe_unused]] usize bytes_received = socket.receive(boost::asio::buffer(buffer, buffer.size()));
        auto& ethernet = (const toobz::EthernetHeader&)buffer;
        if (ethernet.src_addr != filter_addr && ethernet.dst_addr != filter_addr) {
            std::cout << "src=" << ethernet.src_addr << ", dst=" << ethernet.dst_addr << std::endl;
        }
    }

    return 0;
}
