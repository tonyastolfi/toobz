#pragma once
#ifndef TOOBZ_PROTOCOL_ETHERNET_HPP
#define TOOBZ_PROTOCOL_ETHERNET_HPP

#include <toobz/hex.hpp>
#include <toobz/int_types.hpp>

#include <batteries/status.hpp>

#include <array>
#include <string_view>

namespace toobz {

struct __attribute__((packed)) MACAddress {
    static constexpr usize kByteSize = 6;

    static batt::StatusOr<MACAddress> from_str(std::string_view s)
    {
        static_assert(kByteSize == 6, "if address size changes, need to rewrite the code below");

        const auto read_octet = [&s](usize pos) -> batt::StatusOr<u8> {
            BATT_ASSIGN_OK_RESULT(const u8 hi, parse_hex_digit(s[pos + 0]));
            BATT_ASSIGN_OK_RESULT(const u8 lo, parse_hex_digit(s[pos + 1]));

            return {(hi << 4) | lo};
        };
        (void)read_octet;

        MACAddress addr;

        if (s.size() == addr.bytes.size() * 2) {
            usize pos = 0;
            for (u8& addr_byte : addr.bytes) {
                BATT_ASSIGN_OK_RESULT(addr_byte, read_octet(pos));
                pos += 2;
            }
            return addr;

        } else if (s.size() == kByteSize * 2 + (kByteSize - 1) &&  //
                   s[(1 * 3) - 1] == s[(2 * 3) - 1] &&             //
                   s[(2 * 3) - 1] == s[(3 * 3) - 1] &&             //
                   s[(3 * 3) - 1] == s[(4 * 3) - 1] &&             //
                   s[(4 * 3) - 1] == s[(5 * 3) - 1]) {
            usize pos = 0;
            for (u8& addr_byte : addr.bytes) {
                BATT_ASSIGN_OK_RESULT(addr_byte, read_octet(pos));
                pos += 3;
            }
            return addr;
        }
        return {batt::StatusCode::kInvalidArgument};
    }

    std::array<u8, 6> bytes;
};

inline bool operator==(const MACAddress& l, const MACAddress& r)
{
    return l.bytes == r.bytes;
}

inline bool operator!=(const MACAddress& l, const MACAddress& r)
{
    return !(l == r);
}

inline std::ostream& operator<<(std::ostream& out, const MACAddress& t)
{
    std::array<char, sizeof(t) * 3> buffer;
    usize pos = 0;
    for (const u8 addr_byte : t.bytes) {
        buffer[pos + 0] = format_hex_digit(addr_byte >> 4);
        buffer[pos + 1] = format_hex_digit(addr_byte);
        buffer[pos + 2] = ':';
        pos += 3;
    }
    return out << std::string_view{buffer.data(), buffer.size() - 1};
}

struct EthernetHeader {
    MACAddress dst_addr;
    MACAddress src_addr;
    std::array<u8, 4> tag_802_1q;
    big_u16 ether_type;
};

static_assert(sizeof(EthernetHeader) == 18, "");

}  // namespace toobz

#endif  // TOOBZ_PROTOCOL_ETHERNET_HPP
