#pragma once
#ifndef TOOBZ_RAW_SOCKET_HPP
#define TOOBZ_RAW_SOCKET_HPP

#include <toobz/int_types.hpp>

//#include<boost/asio/>
#include <boost/asio/basic_raw_socket.hpp>

#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <sys/socket.h>

#include <cstddef>
#include <string>

namespace toobz {

class RawSocketProtocol;

class RawSocketEndpoint
{
   public:
    /// The protocol type associated with the endpoint.
    using protocol_type = RawSocketProtocol;
    using data_type = struct sockaddr_ll;  // boost::asio::detail::socket_addr_type; ??

    RawSocketEndpoint() noexcept
    {
        std::memset(&this->sockaddr_, 0, sizeof(this->sockaddr_));
    }

    /// Constructor
    explicit RawSocketEndpoint(const std::string& ifname)
    {
        this->sockaddr_.sll_family = PF_PACKET;
        this->sockaddr_.sll_protocol = htons(ETH_P_ALL);
        this->sockaddr_.sll_ifindex = if_nametoindex(ifname.c_str());
        this->sockaddr_.sll_hatype = 1;
    }

    /// Assign from another endpoint.
    RawSocketEndpoint& operator=(const RawSocketEndpoint& other)
    {
        this->sockaddr_ = other.sockaddr_;
        return *this;
    }

    /// The protocol associated with the endpoint.
    protocol_type protocol() const;

    /// Get the underlying endpoint in the native type.
    data_type* data()
    {
        return &this->sockaddr_;
    }

    /// Get the underlying endpoint in the native type.
    const data_type* data() const
    {
        return &this->sockaddr_;
    }

    /// Get the underlying size of the endpoint in the native type.
    usize size() const
    {
        return sizeof(this->sockaddr_);
    }

    /// Set the underlying size of the endpoint in the native type.
    void resize(usize /*size*/)
    {
        /* nothing we can do here */
    }

    /// Get the capacity of the endpoint in the native type.
    usize capacity() const
    {
        return sizeof(this->sockaddr_);
    }

    /// Compare two endpoints for equality.
    friend bool operator==(const RawSocketEndpoint& e1, const RawSocketEndpoint& e2)
    {
        return !std::memcmp(&e1.sockaddr_, &e2.sockaddr_, sizeof(sockaddr_));
    }

    /// Compare two endpoints for inequality.
    friend bool operator!=(const RawSocketEndpoint& e1, const RawSocketEndpoint& e2)
    {
        return !(e1 == e2);
    }

    /// Compare endpoints for ordering.
    friend bool operator<(const RawSocketEndpoint& e1, const RawSocketEndpoint& e2)
    {
        return std::memcmp(&e1.sockaddr_, &e2.sockaddr_, sizeof(sockaddr_));
    }

    /// Compare endpoints for ordering.
    friend bool operator>(const RawSocketEndpoint& e1, const RawSocketEndpoint& e2)
    {
        return e2 < e1;
    }

    /// Compare endpoints for ordering.
    friend bool operator<=(const RawSocketEndpoint& e1, const RawSocketEndpoint& e2)
    {
        return !(e2 < e1);
    }

    /// Compare endpoints for ordering.
    friend bool operator>=(const RawSocketEndpoint& e1, const RawSocketEndpoint& e2)
    {
        return !(e1 < e2);
    }

   private:
    struct sockaddr_ll sockaddr_;
};

class RawSocketProtocol
{
   public:
    using socket = boost::asio::basic_raw_socket<RawSocketProtocol>;
    using endpoint = RawSocketEndpoint;

    explicit RawSocketProtocol(i32 protocol, i32 family) : protocol_{protocol}, family_{family}
    {
    }

    explicit RawSocketProtocol() : RawSocketProtocol{htons(ETH_P_ALL), PF_PACKET}
    {
    }

    i32 type() const
    {
        return SOCK_RAW;
    }

    i32 protocol() const
    {
        return this->protocol_;
    }

    i32 family() const
    {
        return this->family_;
    }

   private:
    i32 protocol_;
    i32 family_;
};

using RawSocket = RawSocketProtocol::socket;

//#=##=##=#==#=#==#===#+==#+==========+==+=+=+=+=+=++=+++=+++++=-++++=-+++++++++++

inline auto RawSocketEndpoint::protocol() const -> protocol_type
{
    return protocol_type{};
}

}  // namespace toobz

#endif  // TOOBZ_RAW_SOCKET_HPP
