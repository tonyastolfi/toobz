#pragma once
#ifndef TOOBZ_HEX_HPP
#define TOOBZ_HEX_HPP

#include <toobz/int_types.hpp>

#include <batteries/status.hpp>

namespace toobz {

inline batt::StatusOr<u8> parse_hex_digit(char ch)
{
    if (ch >= '0' && ch <= '9') {
        return ch - '0';
    }
    if (ch >= 'a' && ch <= 'f') {
        return ch - 'a' + 10;
    }
    if (ch >= 'A' && ch <= 'F') {
        return ch - 'A' + 10;
    }
    return {batt::StatusCode::kInvalidArgument};
}

inline char format_hex_digit(int value)
{
    static constexpr auto digit_chars = "0123456789abcdef";
    return digit_chars[value & 0xf];
}
}  // namespace toobz

#endif  // TOOBZ_HEX_HPP
